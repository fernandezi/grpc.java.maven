package com.grpc.server;

import com.grpc.proto.SumServiceGrpc;

import org.lognet.springboot.grpc.GRpcService;

import com.grpc.proto.Sum.RequestSum;
import com.grpc.proto.Sum.ResponseSum;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@GRpcService//default port 6565
public class SumServiceImpl extends SumServiceGrpc.SumServiceImplBase{
	
	@Override
	public void sumNumbers(RequestSum request,StreamObserver<ResponseSum> response) {
		log.info("Request: {}", request);
		int total = request.getNum1() + request.getNum2();
		ResponseSum responseTotal = ResponseSum.newBuilder().setTotal(total).build();
		
		response.onNext(responseTotal);
		response.onCompleted();
	}

}
