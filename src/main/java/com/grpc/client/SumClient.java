package com.grpc.client;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.grpc.proto.Sum.RequestSum;
import com.grpc.proto.Sum.ResponseSum;
import com.grpc.proto.SumServiceGrpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SumClient {
	
	private SumServiceGrpc.SumServiceBlockingStub sumBlockingStub;
	
	@PostConstruct
	public void init() {
		ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565).usePlaintext().build();
		sumBlockingStub = SumServiceGrpc.newBlockingStub(channel);
	}
	
	public int sumNumbers(int num1, int num2) {
		RequestSum request = RequestSum.newBuilder().setNum1(num1).setNum2(num2).build();
		log.info("Request sumNumbers {}", request);
		
		ResponseSum response = sumBlockingStub.sumNumbers(request);
		log.info("Response sumNumbers {}", response);
		
		return response.getTotal();
	}
	
	
}
