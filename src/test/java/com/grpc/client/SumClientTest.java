package com.grpc.client;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SumClientTest {

	@Autowired
	private SumClient clientSum;
	
	@Test
	public void given_TwoNumbers_When_callSumService_Then_ResponseAccumulatedNumbers() {
		assertEquals(clientSum.sumNumbers(2, 23),25);
	}
}
